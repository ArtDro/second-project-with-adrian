import datetime


class Car:
    def __init__(self, brand, colour, production_year):
        self.brand = brand
        self.colour = colour
        self.production_year = production_year
        self.mileage = 0

    def drive(self, distance):
        self.mileage += distance

    def get_age(self):
        current_year = 2023
        car_age = current_year - int(self.production_year)
        return car_age

    def repaint(self, color):
        self.colour = color


if __name__ == '__main__':
    car1 = Car('Toyota', 'black', '2001')
    car2 = Car('Ford', 'green', '2021')
    car3 = Car('Audi', 'red', '1997')
    color = 'blue'
    print(car2.mileage)
    print(car1.brand, car1.colour)
    print(car3.colour)
    print(car1.get_age())
    print(car2.get_age())
    print(car3.get_age())
    car1.colour = 'blue'
    print(car1.colour)
